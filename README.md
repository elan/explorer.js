# DataExplorer.js

Logiciel produit dans le cadre du projet ANR [L'invention du théâtre antique dans le corpus des paratextes savants du XVIe s. Analyse, traduction, exploration numérique – IThAC](https://anr.fr/Projet-ANR-19-CE27-0009)

Affichage dynamique et hierarchisé de données "plates", avec filtre et tri

Refonte complète du [code de Maxime Bouton et Vincent Maillard (Protocole astral)](https://gitlab.com/litt-arts-num/ithac-visualisations) qui tend maintenant à la généricité.

![Screenshot](screenshot.png "Screenshot").

## Installation
`npm install elan-data-explorer.js`

## Mise à jour
`npm update elan-data-explorer.js`

(attention si la version est fixée dans le fichier `package.json` du projet utilisant l'explorer, il faut le modifier avant)

## Contribution au code
Penser à incrémenter la ligne  `"version": "1.0.xx"` dans le `package.json` de ce projet quand le code change. Il faut aussi publier la nouvelle version avec la commande `npm publish` 

## Utilisation

### Structure de données
Les données en entrée doivent être en JSON, sous cette forme  

```json
[
  {'id': 1, "property_1": "foo", "property_2": "oof 2", "property_3": "ofo"},
  {'id': 2, "property_1": "bar", "property_2": ["oof", "bra"], "property_3": "rab"},
  ...
]
```

### Paramètres 
- Il faut fournir une URL d'API pour les récupérer (voir le paramètre `apiUrl`; /!\ CORS) ou passer les données en javascript (via le paramètre `data`)

- On définit les propriétés qui seront utilisées pour l'affichage hiérarchique via le paramètre `hierarchicalOptions`. (laisser une chaine vide pour le 1er élément)

```js
hierarchicalOptions: [
  "",
  "source_author",
  "source_work"
]
```

- On définit les propriétés qui feront l'objet de filtres via le paramètre `filters`
```js
filters: [
  "source_author", 
  "source_work", 
  "bibl_scope", 
  "fragment_id"
]
```

- On peut traduire les intitulés des propriétés et leurs valeurs via le paramètre `strings`
```js 
strings: {
    "inconnu": "Inconnu",
    "source_author": "Auteur source",
    "source_work": "Oeuvre source",
    "source_genre": "Genre source",
    "citing_author": "Auteur citant"
}
```

- Le paramètre `itemProperties` spécifie quelles propriétés (et dans quel ordre) seront affichées sur les éléments terminaux.

- Le paramètre `itemPropertiesDisplay` permet de choisir d'afficher les propriétés des éléments de façon `inline` ou `block`. Si la valeur est `block`, les titres des propriétés sont affichés.  

- Le paramètre `itemPropertiesHideUndefined` permet de masquer les propriétés pour lesquelles la valeur est `undefined` (`true` or `false`). 

- Le paramètre `linkProperty` permet de spécifier quelle propriété sera utilisée pour afficher le lien vers la ressource.
  
- Les paramètres `sortLeavesProperty` et `sortLeavesDirection` permettent de spécifier la clé de tri des éléments terminaux et la direction du tri (`ASC` ou `DESC`). 

- Le paramètre `leavesAttributes` permet de définir les propriétés pour lesquelles les valeurs seront injectées sous forme de data-attribute au niveau des éléments terminaux.  

- Le paramètre `colors` permet de surcharger certaines couleurs.
```js
colors: {
  "--lvl1": "#f89623",
  "--lvl2": "#d91e5f",
  "--lvl3": "#683191",
},
```

- le paramètre `strOverride` permet de surcharger certaines chaines de caractères de l'interface 
```js
strOverride: {
  addChoice: "Ajouter un choix",
  default: "Commencez par ajouter autant de choix hiérarchiques...",
  filters: "Filtres",
  hierarchicalChoices: "Choix hiérarchiques",
  loading: 'Récupération des données ...',
  options: "Options",
  reinit: "Réinitialiser",
  copyURL: "Copier l'URL",
  undefined: "Non renseigné",
  items: "Items",
  blocks: "Blocs",
  blockSort: "Tri des blocs",
  itemCount: "Nombre d'items"
}
```

### Autre
- le document HTML doit comporter un élément dont l'id est `explorer-wrapper`

### Exemple d'utlisation
#### script.js
```javascript
import { Explorer } from './node_modules/elan-data-explorer.js/index.js'

window.addEventListener("load", (event) => {
  const params = {
    data: [
      {'id': 1, "property_1": "foo", "property_2": "oof 2", "property_3": "ofo"},
      {'id': 2, "property_1": "bar", "property_2": ["oof", "bra"], "property_3": "rab"}
    ],
    apiUrl: null,
    itemBaseUrl: "https://domain.name/item/",
    filters: [
      "property_2", 
      "property_3"
    ],
    hierarchicalOptions: [
      "property_1", 
      "property_2"
    ],
    itemProperties: [
        "property_1", 
        "property_3",
        "id"
    ],
    strings: {
      "property_1": "Propriété 1", 
      "property_2": "Propriété 2", 
      "property_3": "Propriété 3"
    },
    linkProperty: "id",
    colors: {
      "--lvl1":"#FC999B", 
      "--lvl2":"#A0B5C6", 
      "--lvl3":"#FCE09F", 
      "--lvl4":"#C4DCB1", 
      "--lvl5":"#F8B28C", 
      "--lvl6":"#95D5C1", 
      "--lvl7":"#FBC787"
    },
  }
  const e = Object.create(Explorer)
  e.init(params)
});
```

#### index.html
```html
<!DOCTYPE html>
<html>
  <head>
      <meta charset="UTF-8">
      <title>Démo Explorer</title>
      <link href="node_modules/elan-data-explorer.js/style.css" rel="stylesheet">
  </head>
  <body>
      <div id="explorer-wrapper"></div>
      <script src="script.js" type="module"></script>
  </body>
</html>
```

Voir des exemples d'utilisation [ici](https://elan.gricad-pages.univ-grenoble-alpes.fr/demo-explorer)

## Auteur
Arnaud Bey 

## Licence
GNU GENERAL PUBLIC LICENSE V3