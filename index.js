export const Explorer = {
  data: [],
  params: {},
  strings: {
    addChoice: "Ajouter un choix",
    default: "Commencez par ajouter autant de choix hiérarchiques (via le bloc de gauche) que vous voulez pour organiser les éléments ci-dessous.",
    filters: "Filtres",
    hierarchicalChoices: "Choix hiérarchiques",
    loading: 'Récupération des données ...',
    options: "Options",
    reinit: "Tout réinitialiser",
    reinitFilters: "Réinitialiser les filtres",
    copyURL: "Copier l'URL",
    undefined: "Non renseigné",
    items: "Items",
    blocks: "Blocs",
    blockSort: "Tri des blocs",
    itemCount: "Nombre d'items"
  },
  init: function (params) {
    this.params = params
    this.createHtml()
    this.getData().then(data => {
      this.data = JSON.parse(data)
      this.params.filters.forEach(filter => {
        this.generateFilter(filter)
      })
      this.recalculateWrapper()
    });
  },

  hierarchicalChoiceEvent: function () {
    let choices = [...document.querySelectorAll(".hierarchical-choice")].filter((choice) => (choice.value != "")).map(choice => choice.value)
    let search_params = this.getURLparams()
    search_params.delete("hierarchy")
    choices.forEach(param => search_params.append("hierarchy", param));
    this.updateURL(search_params);
  },

  filtersEvents: function () {
    let search_params = this.getURLparams()

    this.params.filters.forEach(filter => {
      search_params.delete(filter)
      document.querySelectorAll("input[data-filter=" + filter + "]:checked").forEach(el => search_params.append(filter, el.value))
      this.updateURL(search_params);
    })
  },

  sortAccordions: function () {
    // retrieve checked radio btn
    let criteria = document.querySelector('input[name="sort"]:checked').value

    // update URL params
    let search_params = this.getURLparams()
    search_params.set("sort", criteria)
    this.updateURL(search_params);

    // sort
    let accordions = [...document.querySelectorAll("#visu .accordion-explorer")]
    accordions.forEach(accordion => {
      this.sort(accordion, criteria, ".accordion-explorer-item", "DESC").forEach(div => accordion.appendChild(div));
    });
  },

  collapseOne: function (btn) {
    btn.classList.toggle("collapsed")
    let target = document.querySelector(btn.dataset.target)
    target.classList.toggle("show")
  },

  collapseAll: function () {
    // retrieve checked radio btn
    let option = document.querySelector('input[name="collapse"]:checked').value

    // update URL params
    let search_params = this.getURLparams()
    search_params.set("collapse", option)
    this.updateURL(search_params);

    let accordionsBtn = [...document.querySelectorAll("#visu .collapse-button")]
    let accordionsCollapse = [...document.querySelectorAll("#visu .accordion-collapse")]

    // collapse
    if (option == "collapse") {
      accordionsBtn.forEach(accordion => {
        accordion.classList.add("collapsed")
      })

      accordionsCollapse.forEach(accordion => {
        accordion.classList.remove("show")
      })
    }
    // uncollapse
    else {
      accordionsBtn.forEach(accordion => {
        accordion.classList.remove("collapsed")
      })

      accordionsCollapse.forEach(accordion => {
        accordion.classList.add("show")
      })
    }

  },

  createHtml: function () {
    if (this.params.colors) {
      for (const [key, value] of Object.entries(this.params.colors)) {
        document.documentElement.style.setProperty(key, value)
      }
    }
    let search_params = this.getURLparams()

    let wrapper = document.querySelector("#explorer-wrapper")

    let modal = document.createElement("div")
    modal.id = "modal"
    let modalContent = document.createElement("div")
    modalContent.id = "modal-content"
    modalContent.innerHTML = "Calcul en cours..."
    modal.appendChild(modalContent)
    document.querySelector("body").appendChild(modal)

    let tools = document.createElement("div")
    tools.id = "tools"
    wrapper.appendChild(tools)

    let choicesTitle = document.createElement("h1")
    choicesTitle.innerHTML = this.strings.hierarchicalChoices
    tools.appendChild(choicesTitle)
    let choices = document.createElement("div")
    choices.id = "hierarchical-choices"
    tools.appendChild(choices)
    let addChoiceBtn = document.createElement("div")
    addChoiceBtn.id = "add-hierarchical-choice"
    addChoiceBtn.innerText = this.strings.addChoice
    addChoiceBtn.addEventListener("click", () => {
      this.addHierarchicalChoice()
    })
    tools.appendChild(addChoiceBtn)

    search_params.getAll("hierarchy").forEach((value) => {
      this.addHierarchicalChoice(value)
    })

    let sortAndFilters = document.createElement("div")
    sortAndFilters.id = "sort-and-filters"
    sortAndFilters.style.display = "none"
    tools.appendChild(sortAndFilters)

    let optionTitle = document.createElement("h1")
    optionTitle.innerHTML = this.strings.options
    sortAndFilters.appendChild(optionTitle)

    /**
     * SORT OPTIONS
     */

    let sortOptions = document.createElement("div")
    sortOptions.id = "sort-options"
    sortOptions.classList.add("option-block")
    sortAndFilters.appendChild(sortOptions)
    let sortOptionsTitle = document.createElement("div")
    sortOptionsTitle.innerText = (this.params.strOverride && this.params.strOverride.blockSort) ? this.params.strOverride.blockSort : this.strings.blockSort
    sortOptionsTitle.classList.add("option-title")
    sortOptions.appendChild(sortOptionsTitle)

    let sortValue = search_params.get("sort") ? search_params.get("sort") : "count"

    let sortAlpha = document.createElement("span")
    sortOptions.appendChild(sortAlpha)
    sortAlpha.addEventListener("click", () => {
      this.sortAccordions()
    })

    let sortAlphaInput = document.createElement("input")
    sortAlphaInput.id = "sort-alpha"
    sortAlphaInput.setAttribute("type", "radio")
    sortAlphaInput.value = "alpha"
    sortAlphaInput.name = "sort"
    sortAlphaInput.checked = (sortValue == "alpha")
    sortAlpha.appendChild(sortAlphaInput)

    let sortAlphalabel = document.createElement("label")
    sortAlphalabel.setAttribute("for", "sort-alpha")
    sortAlphalabel.innerHTML = "Alphabétique"
    sortAlpha.appendChild(sortAlphalabel)

    let sortCount = document.createElement("span")
    sortOptions.appendChild(sortCount)

    let sortCountInput = document.createElement("input")
    sortCountInput.id = "sort-count"
    sortCountInput.setAttribute("type", "radio")
    sortCountInput.value = "count"
    sortCountInput.name = "sort"
    sortCountInput.checked = (sortValue == "count")
    sortCount.appendChild(sortCountInput)

    let sortCountlabel = document.createElement("label")
    sortCountlabel.setAttribute("for", "sort-count")
    sortCountlabel.innerHTML = (this.params.strOverride && this.params.strOverride.itemCount) ? this.params.strOverride.itemCount : this.strings.itemCount
    sortCount.appendChild(sortCountlabel)

    sortCount.addEventListener("click", () => {
      this.sortAccordions()
    })

    /**
    * BLOCK OPTIONS
    */

    let blockOptions = document.createElement("div")
    blockOptions.classList.add("option-block")

    sortAndFilters.appendChild(blockOptions)
    let blockOptionsTitle = document.createElement("div")
    blockOptionsTitle.innerText = (this.params.strOverride && this.params.strOverride.blocks) ? this.params.strOverride.blocks : this.strings.blocks
    blockOptionsTitle.classList.add("option-title")
    blockOptions.appendChild(blockOptionsTitle)

    let collapseValue = search_params.get("collapse") ? search_params.get("collapse") : "uncollapse"

    let collapse = document.createElement("span")
    blockOptions.appendChild(collapse)
    collapse.addEventListener("click", () => {
      this.collapseAll()
    })

    let collapseInput = document.createElement("input")
    collapseInput.id = "collapse-all"
    collapseInput.setAttribute("type", "radio")
    collapseInput.value = "collapse"
    collapseInput.name = "collapse"
    collapseInput.checked = (collapseValue == "collapse")
    collapse.appendChild(collapseInput)

    let collapseLabel = document.createElement("label")
    collapseLabel.setAttribute("for", "collapse-all")
    collapseLabel.innerHTML = "Tout replier"
    collapse.appendChild(collapseLabel)

    let uncollapse = document.createElement("span")
    blockOptions.appendChild(uncollapse)
    uncollapse.addEventListener("click", () => {
      this.collapseAll()
    })

    let uncollapseInput = document.createElement("input")
    uncollapseInput.id = "uncollapse-all"
    uncollapseInput.setAttribute("type", "radio")
    uncollapseInput.value = "uncollapse"
    uncollapseInput.name = "collapse"
    uncollapseInput.checked = (collapseValue == "uncollapse")
    uncollapse.appendChild(uncollapseInput)

    let uncollapseLabel = document.createElement("label")
    uncollapseLabel.setAttribute("for", "uncollapse-all")
    uncollapseLabel.innerHTML = "Tout déplier"
    uncollapse.appendChild(uncollapseLabel)

    /**
    * ITEM OPTIONS
    */

    let itemOptions = document.createElement("div")
    itemOptions.classList.add("option-block")
    sortAndFilters.appendChild(itemOptions)
    let itemOptionsTitle = document.createElement("div")
    itemOptionsTitle.innerText = (this.params.strOverride && this.params.strOverride.items) ? this.params.strOverride.items : this.strings.items
    itemOptionsTitle.classList.add("option-title")
    itemOptions.appendChild(itemOptionsTitle)

    let leavesDisplay = search_params.get("leaves") ? search_params.get("leaves") : "show"

    let leavesShow = document.createElement("span")
    itemOptions.appendChild(leavesShow)
    leavesShow.addEventListener("click", () => {
      uncollapseInput.checked = true
      this.collapseAll()
      this.leavesDisplay()
    })

    let leavesShowInput = document.createElement("input")
    leavesShowInput.id = "leaves-show"
    leavesShowInput.setAttribute("type", "radio")
    leavesShowInput.value = "show"
    leavesShowInput.name = "leaves-display"
    leavesShowInput.checked = (leavesDisplay == "show")
    leavesShow.appendChild(leavesShowInput)

    let leavesShowLabel = document.createElement("label")
    leavesShowLabel.setAttribute("for", "leaves-show")
    leavesShowLabel.innerHTML = "Afficher"
    leavesShow.appendChild(leavesShowLabel)

    let leavesHide = document.createElement("span")
    itemOptions.appendChild(leavesHide)
    leavesHide.addEventListener("click", () => {
      this.leavesDisplay()
    })

    let leavesHideInput = document.createElement("input")
    leavesHideInput.id = "leaves-hide"
    leavesHideInput.setAttribute("type", "radio")
    leavesHideInput.value = "hide"
    leavesHideInput.name = "leaves-display"
    leavesHideInput.checked = (leavesDisplay == "hide")
    leavesHide.appendChild(leavesHideInput)

    let leavesHideLabel = document.createElement("label")
    leavesHideLabel.setAttribute("for", "leaves-hide")
    leavesHideLabel.innerHTML = "Masquer"
    leavesHide.appendChild(leavesHideLabel)

    let btnWrapper = document.createElement("div")
    btnWrapper.id = "btn-wrapper"
    sortAndFilters.appendChild(btnWrapper)

    /**
    * RESET 
    */

    let reset = document.createElement("span")
    reset.innerText = this.strings.reinit
    reset.id = "reset"
    reset.setAttribute("type", "button")
    reset.addEventListener("click", () => {
      this.reset()
    })
    btnWrapper.appendChild(reset)

    let resetFilters = document.createElement("span")
    resetFilters.innerText = this.strings.reinitFilters
    resetFilters.id = "reset-filters"
    resetFilters.setAttribute("type", "button")
    resetFilters.addEventListener("click", () => {
      this.resetFilters()
    })
    btnWrapper.appendChild(resetFilters)

    /**
    * COPY URL 
    */

    let copyUrl = document.createElement("span")
    copyUrl.innerText = this.strings.copyURL
    copyUrl.id = "copy-url"
    copyUrl.setAttribute("type", "button")
    copyUrl.addEventListener("click", () => {
      this.copyUrl()
    })
    btnWrapper.appendChild(copyUrl)

    /**
    * FILTERS
    */

    let filterTitle = document.createElement("h1")
    filterTitle.innerHTML = this.strings.filters
    sortAndFilters.appendChild(filterTitle)
    let filters = document.createElement("div")
    filters.classList.add("accordion-explorer")
    filters.id = "filters"
    sortAndFilters.appendChild(filters)

    /**
    * VISU
    */
    let visu = document.createElement("div")
    visu.id = "visu"
    wrapper.appendChild(visu)
    let accordion = document.createElement("div")
    accordion.classList.add("accordion-body", "global")
    visu.appendChild(accordion)
    accordion.innerHTML = this.strings.loading
  },

  leavesDisplay: function () {
    // retrieve checked radio btn
    let option = document.querySelector('input[name="leaves-display"]:checked').value

    // update URL params
    let search_params = this.getURLparams()
    search_params.set("leaves", option)
    this.updateURL(search_params);

    // Hide or show leaves
    let els = [...document.querySelectorAll("#visu .leaves-wrapper")]

    if (option == "show") {
      els.forEach(el => {
        el.closest(".accordion-collapse").classList.add("show")
        el.closest(".accordion-explorer-item").querySelector(".collapse-button").classList.remove("collapsed")
      })
    } else {
      els.forEach(el => {
        el.closest(".accordion-collapse").classList.remove("show")
        el.closest(".accordion-explorer-item").querySelector(".collapse-button").classList.add("collapsed")
      })
    }
  },

  getDistinctValues: function (property, subset, sort) {
    let values = [...new Set(
      subset.map(item => item[property]).flat()
    )]

    if (sort) {
      let explorer = this
      values.sort(function (a, b) {
        return explorer.getTranslation(a).localeCompare(explorer.getTranslation(b));
      })
    }

    return values
  },

  reset: function () {
    document.querySelectorAll(".hierarchical-choice-wrapper").forEach(e => e.remove())
    document.querySelectorAll("input[data-filter]:checked").forEach(e => e.checked = false)
    document.querySelectorAll("input[type='text']").forEach(e => e.value = "")
    document.querySelector("input[name='sort'][value='count']").checked = true

    let baseUrl = window.location.href.split("?")[0];
    window.history.pushState('object', document.title, baseUrl);

    this.recalculateWrapper()
  },

  resetFilters: function() {
    document.querySelectorAll("input[data-filter]:checked").forEach(e => e.checked = false)
    document.querySelectorAll("input[type='text']").forEach(e => e.value = "")
    document.querySelectorAll("input[type='text']").forEach(e => e.value = "")
    // on réaffiche toutes les checkbox
    document.querySelectorAll(".filter div[style*=\"display: none\"], .filter div[style*=\"display:none\"]").forEach(e => e.style.display = "block")
    // on replit les filtres ouverts
    document.querySelectorAll("#filters .accordion-explorer-item .show").forEach(e => e.classList.remove("show"))

    this.filtersEvents()
    this.recalculateWrapper()
  },

  /**
  * check unchecked and visible checkboxes for a given metadata filter
  */
  checkVisible: function(btn) {
    let metadata = btn.dataset.filter
    let checkboxes = document.querySelectorAll(".filter[data-filter='"+metadata+"'] div:not([style*=\"display: none\"]):not([style*=\"display:none\"]) input[type='checkbox']:not(:checked)")
    
    if(checkboxes.length > 0) {
      checkboxes.forEach((checkbox) => {
        checkbox.checked = true
      })
      this.filtersEvents()
      this.recalculateWrapper()
    }
  },

  /**
  * uncheck checked and visible checkboxes for a given metadata filter
  */
  uncheckVisible: function(btn) {
    let metadata = btn.dataset.filter
    let checkboxes = document.querySelectorAll(".filter[data-filter='"+metadata+"'] div:not([style*=\"display: none\"]):not([style*=\"display:none\"]) input[type='checkbox']:checked")
    
    if(checkboxes.length > 0) {
      checkboxes.forEach((checkbox) => {
        checkbox.checked = false
      })
      this.filtersEvents()
      this.recalculateWrapper()
    }
  },

  textFilter: function (input) {
    let metadata = input.dataset.filter
    let query = input.value.toUpperCase()
    document.querySelectorAll("input[type='checkbox'][data-filter='" + metadata + "']").forEach((checkbox) => {

      if (checkbox.value.toUpperCase().includes(query) || checkbox.dataset.translation.toUpperCase().includes(query) || checkbox.checked) {
        checkbox.parentNode.style.display = "block"
      } else {
        checkbox.parentNode.style.display = "none"
      }

    })
  },

  generateFilter: function (metadata) {
    // Prepare an array to apply url filters
    let search_params = this.getURLparams()
    let urlValues = []
    search_params.getAll(metadata).forEach((value) => {
      urlValues.push(value)
    })

    let values = this.getDistinctValues(metadata, this.data, true)

    let filter = document.createElement("div")
    filter.classList.add("filter")
    filter.dataset.filter = metadata

    // input text permettant de filter les checkbox
    if (values.length > 5) {
      let inputTextFilter = document.createElement("input")
      inputTextFilter.setAttribute("type", "text")
      inputTextFilter.setAttribute("placeholder", "Filtrer")
      inputTextFilter.classList.add("text-filter")
      inputTextFilter.dataset.filter = metadata

      inputTextFilter.addEventListener("keyup", (event) => {
        if (event.isComposing || event.keyCode === 229) {
          return;
        }
        this.textFilter(inputTextFilter)
      });

      filter.appendChild(inputTextFilter)
    }

    // bouton permettant de cocher les checkbox visibles
    let btnCheckVisible = document.createElement("span")
    btnCheckVisible.setAttribute("type", "button")
    btnCheckVisible.innerText = "cocher visibles"
    btnCheckVisible.dataset.filter = metadata

    filter.appendChild(btnCheckVisible)
    btnCheckVisible.addEventListener("click", (event) => {
      this.checkVisible(btnCheckVisible)
    });

    // bouton permettant de décocher les checkbox visibles
    let btnUncheckVisible = document.createElement("span")
    btnUncheckVisible.setAttribute("type", "button")
    btnUncheckVisible.innerText = "décocher visibles"
    btnUncheckVisible.dataset.filter = metadata

    filter.appendChild(btnUncheckVisible)
    btnUncheckVisible.addEventListener("click", (event) => {
      this.uncheckVisible(btnUncheckVisible)
    });

    let i = 0
    values.forEach(value => {
      i++
      let id = metadata + "_" + i
      let wrapper = document.createElement("div")

      let translatedValue = this.getTranslation(value)
      let input = document.createElement("input")
      input.type = "checkbox"
      input.value = value
      input.dataset.translation = translatedValue
      input.id = id
      input.dataset.filter = metadata
      if (urlValues.includes(value)) {
        input.setAttribute("checked", true)
      }

      input.addEventListener("change", () => {
        this.filtersEvents()
        this.recalculateWrapper()
      })

      wrapper.appendChild(input)

      let label = document.createElement("label")
      label.innerHTML = translatedValue
      label.title = value
      label.setAttribute("for", id)
      wrapper.appendChild(label)

      filter.appendChild(wrapper)
    });

    let accordionItem = this.createAccordionItem("filter_" + metadata, metadata, false, false, true)
    accordionItem.querySelector('.accordion-body').appendChild(filter)

    document.querySelector("#filters").appendChild(accordionItem)
  },

  getTranslation: function (key) {
    if (this.params.strings[key]) {
      return this.params.strings[key]
    }

    if (!key) {
      return this.strings.undefined
    }

    return key
  },

  applyFilter: function () {
    return this.data.filter(element => {
      let keepIt = true
      this.params.filters.forEach(filter => {
        if (keepIt) {
          if (this.hasToBeKept(element, filter) == false) {
            keepIt = false
          }
        }
      });

      return (keepIt)
    });
  },

  // renvoie la liste des valeurs cochées pour un filtre donné
  getFilterValues: function (filter) {
    return [...document.querySelectorAll("input[data-filter=" + filter + "]:checked")].map((filter) => {
      return filter.value;
    });
  },

  hasToBeKept: function (el, filter) {
    let checkFilters = this.getFilterValues(filter)
    let AtLeastOne = checkFilters.length > 0 ? true : false

    // on teste si au moins un des élements du filtre à été coché.
    // si aucun filtre on garde l'élément
    if (!AtLeastOne) {
      return true
    }

    // on teste si l'élément à la propriété en question
    if (!el.hasOwnProperty(filter)) {
      el[filter] = "undefined"
    }

    let isArray = Array.isArray(el[filter])
    // si présence, 2 possibilités pour exclure l'élement : 
    // - c'est une propriétée simple (str, int), et on verifie que la valeur de la propriété de l'élément n'est pas comprise dans les filtres cochés
    // - c'est un tableau, on vérifie qu'aucun élement (!some) du tableau n'est présent dans les filtres cochés 
    if (!isArray && (!checkFilters.includes(el[filter].toString()))) {
      return false
    }

    if (isArray && !el[filter].some((e) => checkFilters.includes(e))) {
      return false
    }

    return true
  },

  recalculateWrapper: async function () {
    document.querySelector("#modal").style.display = "block"
    await new Promise(r => setTimeout(r, 50))
    await this.recalculate()
    document.querySelector("#modal").style.display = "none"

    return
  },

  recalculate: async function () {
    let visu = document.querySelector("#visu").querySelector('.accordion-body')
    visu.innerHTML = ""
    let choices = [...document.querySelectorAll(".hierarchical-choice")].filter((choice) => choice.value != "")

    if (choices.length > 0) {
      let subset = this.applyFilter()

      let parent = document.querySelector("#visu")
      let index = 0
      let name = "accordion_"

      this.recursiveAccordion(choices, index, parent, subset, name)
      this.sortAccordions()
      this.collapseAll()
      this.leavesDisplay()

      document.querySelector("#sort-and-filters").style.display = "block"
    } else {
      let defaultWrapper = document.createElement("p")
      defaultWrapper.innerHTML = this.strings.default
      defaultWrapper.id = "default-wrapper"

      visu.appendChild(defaultWrapper)

      document.querySelector("#sort-and-filters").style.display = "none"

      // todo -> factoriser ça
      let leavesWrapper = document.createElement("div")
      leavesWrapper.classList.add("leaves-wrapper")

      this.data.forEach(element => {
        this.createLeave(element, leavesWrapper)
      });
      // end todo
      visu.appendChild(leavesWrapper)
    }

    return
  },

  createLeave: function (element, leavesWrapper) {
    let leave = document.createElement("div")
    leave.classList.add("leave")

    if (this.params.sortLeavesProperty) {
      leave.dataset.sorting = element[this.params.sortLeavesProperty]
    }

    if (this.params.leavesAttributes) {
      this.params.leavesAttributes.forEach(attr => {
        leave.dataset[attr] = element[attr]
      });
    }

    this.params.itemProperties.forEach(prop => {

      if (this.params.itemPropertiesHideUndefined != true || (this.params.itemPropertiesHideUndefined == true && element[prop] != undefined) ) {
    
        let translatedValue = this.getTranslation(element[prop])
        let valueWrapper = document.createElement("span")
        valueWrapper.innerHTML = translatedValue
        valueWrapper.title = translatedValue

        if (this.params.linkProperty != prop) {

          let leavePropertyWrapperTag = (this.params.itemPropertiesDisplay == "block") ? "div" : "span"
          let propEl = document.createElement(leavePropertyWrapperTag)
          propEl.classList.add("leave-" + prop)

          if (this.params.itemPropertiesDisplay == "block") {
            let leavePropertyLabel = document.createElement("span")
            leavePropertyLabel.innerText = this.getTranslation(prop)
            leavePropertyLabel.classList.add("leave-property-label")
            propEl.appendChild(leavePropertyLabel)
          }

          propEl.appendChild(valueWrapper)
          leave.appendChild(propEl)
        } else {
          let leaveLink = document.createElement("a")
          leaveLink.href = this.params.itemBaseUrl + element[prop]
          leaveLink.target = "_blank"
          leaveLink.innerHTML = translatedValue
          leave.appendChild(leaveLink)
        }
      }

      leavesWrapper.appendChild(leave)

    });

    return
  },

  recursiveAccordion: function (choices, index, parent, subset, name) {
    let metadata = choices[index].value
    let accordion = this.createAccordion(metadata, index)
    let distinctValues = this.getDistinctValues(metadata, subset, false)
    let i = 0
    for (const value of distinctValues) {
      let previousCount = subset.length
      i++
      let newSubset = subset.filter(function (el) {
        if (!Array.isArray(el[metadata])) {
          return el[metadata] == value
        } else {
          // todo : vérifier que, si le filtre a été coché, la valeur fait partie des choses cochées ??
          return el[metadata].includes(value)
        }

      })
      let newName = name + "_" + i + "_"
      let accordionItem = this.createAccordionItem(newName, value, newSubset.length, previousCount, false)
      accordion.appendChild(accordionItem)
      if (index + 1 < [...document.querySelectorAll(".hierarchical-choice")].filter((choice) => choice.value != "").length) {
        this.recursiveAccordion(choices, index + 1, accordionItem, newSubset, newName)
      } else {
        let leavesWrapper = document.createElement("div")
        leavesWrapper.classList.add("leaves-wrapper")

        newSubset.forEach(element => {
          // todo -> factoriser
          this.createLeave(element, leavesWrapper)
        })

        if (this.params.sortLeavesProperty) {
          this.sort(leavesWrapper, "sorting", ".leave", this.params.sortLeavesDirection).forEach(leave => leavesWrapper.appendChild(leave));
        }

        accordionItem.querySelector('.accordion-body').appendChild(leavesWrapper)
      }
    }

    parent.querySelector('.accordion-body').appendChild(accordion)
  },

  removeHierarchicalChoice: function (deleteBtn) {
    deleteBtn.parentNode.remove()
    this.recalculateWrapper()
  },

  addHierarchicalChoice: function (value = null) {
    let wrapper = document.createElement("div")
    wrapper.classList.add("hierarchical-choice-wrapper")

    // LEVEL BUTTONS
    let lvlBtns = document.createElement("div")
    lvlBtns.classList.add("buttons-level")
    let lvlUp = document.createElement("button")
    lvlUp.classList.add("lvl-up")
    lvlUp.addEventListener("click", () => { this.levelUp(lvlUp) });
    let lvlDown = document.createElement("button")
    lvlDown.classList.add("lvl-down")
    lvlDown.addEventListener("click", () => { this.levelDown(lvlUp) });
    lvlBtns.appendChild(lvlUp)
    lvlBtns.appendChild(lvlDown)
    wrapper.appendChild(lvlBtns)

    // SELECT 
    let select = document.createElement("select")
    select.classList.add("hierarchical-choice")
    wrapper.appendChild(select)

    this.params.hierarchicalOptions.forEach(hierarchicalOption => {
      let value = hierarchicalOption ? this.getTranslation(hierarchicalOption) : ""
      let option = document.createElement("option")
      option.value = hierarchicalOption
      option.innerText = value
      select.appendChild(option)
    });

    if (value) select.value = value

    select.addEventListener("change", () => {
      this.hierarchicalChoiceEvent()
      this.recalculateWrapper()
    })

    // DELETE BUTTON
    let deleteBtn = document.createElement("div")
    deleteBtn.classList.add("delete-choice")
    deleteBtn.addEventListener("click", () => {
      this.removeHierarchicalChoice(deleteBtn)
      this.hierarchicalChoiceEvent()
    })

    wrapper.appendChild(deleteBtn)

    document.querySelector("#hierarchical-choices").appendChild(wrapper)
  },

  levelUp: function (btn) {
    let wrapper = btn.closest(".hierarchical-choice-wrapper")
    let previousWrapper = wrapper.previousElementSibling
    if (previousWrapper) {
      let choicesWrapper = document.querySelector("#hierarchical-choices");
      choicesWrapper.insertBefore(wrapper, previousWrapper);
      this.hierarchicalChoiceEvent()
      this.recalculateWrapper()
    }

    return
  },

  levelDown: function (btn) {
    let wrapper = btn.closest(".hierarchical-choice-wrapper")
    let nextWrapper = wrapper.nextElementSibling
    if (nextWrapper) {
      let nextNextWrapper = nextWrapper.nextElementSibling;
      let choicesWrapper = document.querySelector("#hierarchical-choices");
      choicesWrapper.insertBefore(wrapper, nextNextWrapper);
      this.hierarchicalChoiceEvent()
      this.recalculateWrapper()
    }

    return
  },

  createAccordion: function (metadata, index) {
    let accordion = document.createElement("div")
    accordion.classList.add("accordion-explorer", "accordion-flush", "level-" + index)

    return accordion
  },

  createAccordionItem: function (id, value, count, previousCount, collapsed) {
    let accordionItem = document.createElement("div")
    accordionItem.classList.add("accordion-explorer-item")
    accordionItem.dataset.count = count
    accordionItem.dataset.alpha = value
    accordionItem.dataset.translation = this.getTranslation(value)

    let filterTitle = document.createElement("div")
    accordionItem.appendChild(filterTitle)

    let button = document.createElement("button")
    button.classList.add("collapse-button")
    button.setAttribute("data-target", "#" + id)
    button.innerText = this.getTranslation(value)

    button.addEventListener("click", () => { this.collapseOne(button) });

    if (collapsed) {
      button.classList.add("collapsed")
    }

    if (count != false) {
      let leavesCount = document.createElement("span")
      const percent = count / previousCount * 100
      leavesCount.classList.add("leave-count")
      let str = (this.params.strOverride && this.params.strOverride.items) ? this.params.strOverride.items : this.strings.items
      leavesCount.innerText = count + " " + str.toLowerCase() + " - " + percent.toFixed(1) + "%"
      button.appendChild(leavesCount)
    }

    filterTitle.appendChild(button)

    let contentWrapper = document.createElement("div")
    contentWrapper.id = id
    contentWrapper.classList.add("accordion-collapse", "collapse")

    if (!collapsed) {
      contentWrapper.classList.add("show")
    }

    accordionItem.appendChild(contentWrapper)
    let content = document.createElement("div")
    content.classList.add("accordion-body")
    contentWrapper.appendChild(content)

    return accordionItem
  },

  sort: function (wrapper, dataAttribute, itemSelector, direction = "DESC") {

    let elements = Array.from(wrapper.querySelectorAll(":scope > " + itemSelector))
    return elements.sort(function (a, b) {
      let aValue = a.dataset[dataAttribute]
      let bValue = b.dataset[dataAttribute]

      if (dataAttribute == "count") {
        if (direction == "ASC") {
          return aValue - bValue
        } else {
          return bValue - aValue
        }

      } else if (dataAttribute == "sorting") {
        if (direction == "ASC") {
          return aValue.localeCompare(bValue)
        } else {
          return bValue.localeCompare(aValue)
        }
      }

      else {
        return a.dataset.translation.localeCompare(b.dataset.translation)
      }

    });
  },

  getData: async function () {
    const options = {
      method: 'GET',
      mode: "cors",
    };

    if (this.params.apiUrl != null) {
      return await fetch(this.params.apiUrl, options)
        .then((response) => {
          return response.text()
        })
    } else {
      return JSON.stringify(this.params.data)
    }

  },

  updateURL: function (searchParams) {
    if (searchParams === undefined) var searchParams = this.getURLparams()
    var url = this.getURL()
    if (history.pushState) {
      var newurl = url.origin + url.pathname + "?" + searchParams.toString()
      history.replaceState({
        path: newurl
      }, '', newurl)
    }
  },

  getURL: function () {
    return new URL(document.location.href)
  },

  getURLparams: function () {
    var url = this.getURL()
    var searchParams = new URLSearchParams(url.search);

    return searchParams;
  },

  copyUrl: function () {
    navigator.clipboard.writeText(window.location.href);

    return
  }
}